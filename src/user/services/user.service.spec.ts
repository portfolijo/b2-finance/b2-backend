import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { randomUUID } from 'crypto';
import { NotFoundException } from '@nestjs/common';
import { UpdateUserDto } from '../dto/update-user.dto';

describe('UserService', () => {
  const now = new Date();
  const REPO_TOKEN = getRepositoryToken(User);

  let svc: UserService;
  let repo: Repository<User>;

  let savedUser: User;
  let allUsers: User[];

  beforeEach(async () => {
    savedUser = {
      id: randomUUID(),
      username: 'a',
      email: 'a@email.com',
      firstName: 'first',
      lastName: 'last',
      createDate: now,
      updateDate: now
    };

    allUsers = [];
    for (let i = 0; i < 3; i++) {
      allUsers.push({
        id: randomUUID(),
        username: i.toString(),
        email: `${i}@email.com`,
        firstName: `first${i}`,
        lastName: `last${i}`,
        createDate: now,
        updateDate: now
      });
    }

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: REPO_TOKEN,
          useValue: {
            save: jest.fn(async (dto: CreateUserDto) => {
              return {
                ...dto,
                id: randomUUID()
              };
            }),

            find: jest.fn(async () => allUsers),

            findOneBy: jest.fn(async ({ id }: { id: string }) => {
              if (id === savedUser.id) return savedUser;
              else return null;
            }),

            update: jest.fn(),

            delete: jest.fn()
          }
        }
      ]
    }).compile();

    svc = module.get<UserService>(UserService);
    repo = module.get<Repository<User>>(REPO_TOKEN);
  });

  it('service should be defined', () => {
    expect(svc).toBeDefined();
  });

  it('repository should be defined', () => {
    expect(repo).toBeDefined();
  });

  describe('create()', () => {
    it('should call repo.save with correct arguments', async () => {
      const dto: CreateUserDto = {
        username: 'x',
        email: 'x@email.com',
        firstName: 'xyz',
        lastName: '123'
      };
      await svc.create(dto);
      expect(repo.save).toBeCalledWith(dto);
    });
  });

  describe('findAll()', () => {
    it('should call repo.find', async () => {
      await svc.findAll();
      expect(repo.find).toBeCalled();
    });
  });

  describe('findOne()', () => {
    it('should call repo.findOneBy with correct arguments', async () => {
      const id = savedUser.id;
      await svc.findOne(id);
      expect(repo.findOneBy).toBeCalledWith({ id });
    });

    it('should throw not found exception if id does not exist', () => {
      let id: string;
      while (!id || id === savedUser.id) id = randomUUID();

      expect(async () => await svc.findOne(id)).rejects.toThrow(
        NotFoundException
      );
    });

    it('should display not found message if id does not exist', () => {
      let id: string;
      while (!id || id === savedUser.id) id = randomUUID();

      /*
        toThrow checks that the thrown error message contains the string,
        rather than if the strings are the same, so passing a regex to test
        exact string match
      */
      expect(async () => await svc.findOne(id)).rejects.toThrow(
        /^User does not exist.$/
      );
    });
  });

  describe('update()', () => {
    it('should call repo.update with correct arguments', async () => {
      const id = savedUser.id;
      const dto: UpdateUserDto = {
        username: 'xyz'
      };
      await svc.update(id, dto);
      expect(repo.update).toHaveBeenCalledWith(id, dto);
    });
  });

  describe('remove()', () => {
    it('should call repo.delete with correct arguments', async () => {
      const id = savedUser.id;
      await svc.remove(id);
      expect(repo.delete).toHaveBeenCalledWith(id);
    });
  });
});
