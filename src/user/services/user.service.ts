import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

/**
 * Provides services for manipulating user entities
 */
@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(@InjectRepository(User) private repo: Repository<User>) {}

  /**
   * Creates a new user
   *
   * @param dto A create user DTO
   * @returns The created user
   */
  async create(dto: CreateUserDto): Promise<User> {
    return await this.repo.save(dto);
  }

  /**
   * Finds all users
   *
   * @returns A list of users
   */
  async findAll(): Promise<User[]> {
    return this.repo.find();
  }

  /**
   * Finds the identified user
   *
   * @param id A user id
   * @returns The identified user, if it exists
   */
  async findOne(id: string): Promise<User> {
    const user = await this.repo.findOneBy({ id });

    if (!user) {
      const message = 'User does not exist.';
      this.logger.error(message);
      throw new NotFoundException(message);
    }

    return user;
  }

  /**
   * Updates the identified user
   *
   * @param id A user id
   * @param dto An update user DTO
   */
  async update(id: string, dto: UpdateUserDto): Promise<void> {
    await this.repo.update(id, dto);
  }

  /**
   * Deletes the identified user
   *
   * @param id A user id
   */
  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }
}
