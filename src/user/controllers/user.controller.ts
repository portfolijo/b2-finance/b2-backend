import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
  Logger
} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { UserDto } from '../dto/user.dto';

/**
 * Handles requests for user entities
 */
@Controller('users')
export class UserController {
  private readonly logger = new Logger(UserController.name);

  constructor(private readonly svc: UserService) {}

  /**
   * Creates a new user
   *
   * @param dto A create user DTO
   * @returns The created user
   */
  @Post()
  async create(@Body() dto: CreateUserDto): Promise<UserDto> {
    this.logger.log(`Create user: ${JSON.stringify(dto)}.`);
    const user = await this.svc.create(dto);
    return new UserDto(user);
  }

  /**
   * Finds all users
   *
   * @returns A list of users
   */
  @Get()
  async findAll(): Promise<UserDto[]> {
    this.logger.log('Find all users.');
    const users = await this.svc.findAll();
    return users?.map((user) => new UserDto(user));
  }
  /**
   * Finds the identified user
   *
   * @param id A user id
   * @returns The identified user, if it exists
   */
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<UserDto> {
    this.logger.log(`Find user with id=${id}.`);
    const user = await this.svc.findOne(id);
    return new UserDto(user);
  }

  /**
   * Updates the identified user
   *
   * @param id A user id
   * @param dto An update user DTO
   * @returns void
   */
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() dto: UpdateUserDto
  ): Promise<void> {
    this.logger.log(
      `Update user with id=${id}, updates=${JSON.stringify(dto)}.`
    );
    return this.svc.update(id, dto);
  }

  /**
   * Deletes the identified user
   *
   * @param id A user id
   * @returns void
   */
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async remove(@Param('id') id: string): Promise<void> {
    this.logger.log(`Delete user with id=${id}.`);
    return this.svc.remove(id);
  }
}
