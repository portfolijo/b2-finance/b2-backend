import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from '../services/user.service';
import { User } from '../entities/user.entity';
import { randomUUID } from 'crypto';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';

describe('UserController', () => {
  const now = new Date();

  let con: UserController;
  let svc: UserService;

  let savedUser: User;

  beforeEach(async () => {
    savedUser = {
      id: randomUUID(),
      username: 'a',
      email: 'a@email.com',
      firstName: 'first',
      lastName: 'last',
      createDate: now,
      updateDate: now
    };

    const UserServiceProvider = {
      provide: UserService,
      useFactory: () => ({
        create: jest.fn(),
        findAll: jest.fn(),
        findOne: jest.fn(),
        update: jest.fn(),
        remove: jest.fn()
      })
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService, UserServiceProvider]
    }).compile();

    con = module.get<UserController>(UserController);
    svc = module.get<UserService>(UserService);
  });

  it('controller should be defined', () => {
    expect(con).toBeDefined();
  });

  it('service should be defined', () => {
    expect(svc).toBeDefined();
  });

  describe('create()', () => {
    it('should call svc.create with correct arguments', () => {
      const dto: CreateUserDto = {
        username: 'x',
        email: 'x@email.com',
        firstName: 'xyz',
        lastName: '123'
      };
      con.create(dto);
      expect(svc.create).toBeCalledWith(dto);
    });
  });

  describe('findAll()', () => {
    it('should call svc.findAll', () => {
      con.findAll();
      expect(svc.findAll).toBeCalled();
    });
  });

  describe('findOne()', () => {
    it('should call svc.findOne with correct arguments', () => {
      const id = randomUUID();
      con.findOne(id);
      expect(svc.findOne).toBeCalledWith(id);
    });
  });

  describe('update()', () => {
    it('should call svc.update with correct arguments', () => {
      const id = savedUser.id;
      const dto: UpdateUserDto = {
        username: 'xyz'
      };
      con.update(id, dto);
      expect(svc.update).toBeCalledWith(id, dto);
    });
  });

  describe('remove()', () => {
    it('should call svc.remove with correct arguments', () => {
      const id = savedUser.id;
      con.remove(id);
      expect(svc.remove).toBeCalledWith(id);
    });
  });
});
