import { OmitType } from '@nestjs/mapped-types';
import { User } from '../entities/user.entity';

/**
 * Represents an application user
 */
export class UserDto extends OmitType(User, ['createDate', 'updateDate']) {
  constructor(user?: User) {
    super();
    this.id = user?.id;
    this.username = user?.username;
    this.email = user?.email;
    this.firstName = user?.firstName;
    this.lastName = user?.lastName;
  }
}
