import { OmitType } from '@nestjs/mapped-types';
import { UserDto } from './user.dto';

/**
 * Contains data for creating a new user
 */
export class CreateUserDto extends OmitType(UserDto, ['id']) {}
