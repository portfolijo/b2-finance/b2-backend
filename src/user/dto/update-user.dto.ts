import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

/**
 * Contains data for updating an existing user
 */
export class UpdateUserDto extends PartialType(CreateUserDto) {}
