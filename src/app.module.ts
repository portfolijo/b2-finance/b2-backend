import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `env/.env.${process.env.NODE_ENV}`,
      isGlobal: true
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) =>
        ({
          type: config.get<string>('DATABASE_TYPE'),
          host: config.get<string>('DATABASE_HOST'),
          port: config.get<number>('DATABASE_PORT'),
          username: config.get<string>('DATABASE_USER'),
          password: config.get<string>('DATABASE_PASS'),
          database: config.get<string>('DATABASE_NAME'),
          entityPrefix: 'db_',
          autoLoadEntities: true,
          synchronize: process.env.NODE_ENV === 'prod' ? false : true
        } as TypeOrmModuleOptions)
    }),
    UserModule
  ]
})
export class AppModule {}
