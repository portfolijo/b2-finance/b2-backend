import { Test, TestingModule } from '@nestjs/testing';
import { UserModule } from 'src/user/user.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from 'src/user/entities/user.entity';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { Repository } from 'typeorm';
import * as supertest from 'supertest';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { randomUUID } from 'crypto';
import { AppModule } from 'src/app.module';
import { UserDto } from 'src/user/dto/user.dto';

/**
 * Checks that two lists of users/DTOs are equivalent
 *
 * @param userDtos A list of user DTOs
 * @param otherDtos A list of create/update user DTOs
 * @returns True if all pairs userDtos[i] and otherDtos[i] are equivalent, or false otherwise
 */
const usersAreEquivalentToDtos = (
  userDtos: UserDto[],
  otherDtos: any[]
): boolean => {
  if (userDtos.length !== otherDtos.length) return false;

  for (let i = 0; i < otherDtos.length; i++) {
    if (
      userDtos[i].id === undefined ||
      userDtos[i].id === null ||
      userDtos[i].username !== otherDtos[i].username ||
      userDtos[i].email !== otherDtos[i].email ||
      userDtos[i].firstName !== otherDtos[i].firstName ||
      userDtos[i].lastName !== otherDtos[i].lastName
    )
      return false;
  }
  return true;
};

/**
 * Generates a list of n CreateUserDtos
 *
 * @param n The desired number of DTOs
 * @returns A list of CreateUserDtos if n > 1, a single DTO if n = 1, or null if n = 0
 */
const generateCreateUserDtos = (n: number): CreateUserDto | CreateUserDto[] => {
  const dtos: CreateUserDto[] = [];

  for (let i = 0; i < n; i++) {
    dtos.push({
      username: i.toString(),
      email: `${i}@email.com`,
      firstName: `first${i}`,
      lastName: `last${i}`
    });
  }

  switch (n) {
    case 0:
      return null;
    case 1:
      return dtos[0];
    default:
      return dtos;
  }
};

describe('User e2e', () => {
  let app: INestApplication;
  let repo: Repository<User>;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, UserModule]
    }).compile();

    app = module.createNestApplication();
    repo = module.get(getRepositoryToken(User));
    await app.init();
  });

  afterEach(async () => {
    await repo.query('DELETE FROM db_user;');
  });

  afterAll(async () => {
    await app.close();
  });

  describe('POST /users', () => {
    it('should return 201 response if successful', async () => {
      const dto = generateCreateUserDtos(1);

      await supertest
        .agent(app.getHttpServer())
        .post('/users')
        .send(dto)
        .expect(HttpStatus.CREATED);
    });

    it('should return the created user', async () => {
      const dto = generateCreateUserDtos(1);

      const { body: user } = await supertest
        .agent(app.getHttpServer())
        .post('/users')
        .send(dto);

      expect(usersAreEquivalentToDtos([user], [dto])).toBe(true);
    });
  });

  describe('GET /users', () => {
    it('should return 200 response', async () => {
      const dtos = generateCreateUserDtos(3) as CreateUserDto[];
      await repo.save(dtos);

      await supertest
        .agent(app.getHttpServer())
        .get('/users')
        .expect(HttpStatus.OK);
    });

    it('should return the correct list of users', async () => {
      const dtos = generateCreateUserDtos(3) as CreateUserDto[];
      await repo.save(dtos);

      const { body: users } = await supertest
        .agent(app.getHttpServer())
        .get('/users');

      expect(usersAreEquivalentToDtos(users, dtos)).toBe(true);
    });

    it('should return empty list if no results', async () => {
      const { body: users } = await supertest
        .agent(app.getHttpServer())
        .get('/users');

      expect(users.length).toBe(0);
    });
  });

  describe('GET /users/:id', () => {
    it('should return 200 response if user exists', async () => {
      const dto = generateCreateUserDtos(1) as CreateUserDto;
      const user = await repo.save(dto);

      await supertest
        .agent(app.getHttpServer())
        .get(`/users/${user.id}`)
        .expect(HttpStatus.OK);
    });

    it('should return 404 response if user does not exist', async () => {
      await supertest
        .agent(app.getHttpServer())
        .get(`/users/${randomUUID()}`)
        .expect(HttpStatus.NOT_FOUND);
    });

    it('should return the correct user', async () => {
      const dto = generateCreateUserDtos(1) as CreateUserDto;
      const expected = await repo.save(dto);

      const { body: actual } = await supertest
        .agent(app.getHttpServer())
        .get(`/users/${expected.id}`);

      expect(actual.id).toBe(expected.id);
    });
  });

  describe('PATCH /users/:id', () => {
    it('should return 200 response if successful', async () => {
      const dto = generateCreateUserDtos(1) as CreateUserDto;
      const user = await repo.save(dto);

      await supertest
        .agent(app.getHttpServer())
        .patch(`/users/${user.id}`)
        .send({ username: 'xyz' })
        .expect(HttpStatus.OK);
    });

    it('should update the database record', async () => {
      const dto = generateCreateUserDtos(1) as CreateUserDto;
      const user = await repo.save(dto);

      const newUsername = `${user.username}xyz`;
      await supertest
        .agent(app.getHttpServer())
        .patch(`/users/${user.id}`)
        .send({ username: newUsername });

      const actual = await repo.findOneBy({ id: user.id });
      expect(actual.username).toBe(newUsername);
    });
  });

  describe('DELETE /users/:id', () => {
    it('should return 204 response', async () => {
      const dto = generateCreateUserDtos(1) as CreateUserDto;
      const user = await repo.save(dto);

      await supertest
        .agent(app.getHttpServer())
        .delete(`/users/${user.id}`)
        .expect(HttpStatus.NO_CONTENT);
    });

    it('should remove the database record', async () => {
      const dto = generateCreateUserDtos(1) as CreateUserDto;
      const user = await repo.save(dto);

      await supertest.agent(app.getHttpServer()).delete(`/users/${user.id}`);

      const actual = await repo.findOneBy({ id: user.id });
      expect(actual).toBeNull();
    });
  });
});
